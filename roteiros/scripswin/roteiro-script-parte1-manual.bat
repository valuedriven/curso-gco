?echo 'Preparando containeres ...'
docker-compose up -d

echo 'Criando estrutura de diretórios para build...'
mkdir .build\lib
mkdir .build\META-INF
mkdir .build\script
mkdir .build\WEB-INF\classes\META-INF
mkdir .build\WEB-INF\lib

echo 'Copiando dependências...'
copy src\main\webapp\index.html        .build\
copy src\main\webapp\produto.html      .build\
copy src\main\webapp\WEB-INF\web.xml   .build\WEB-INF
copy src\main\webapp\lib\angular.js    .build\lib
copy src\main\webapp\script\produto.js .build\script
copy src\main\resources\META-INF\persistence.xml .build\WEB-INF\classes\META-INF
copy bibliotecas .build\WEB-INF\lib\

echo "Compilando codigo fonte..."
docker-compose run --rm java javac -verbose -classpath 'bibliotecas/*'  -d '.build/WEB-INF/classes' src/main/java/br/com/conchayoro/entity/Produto.java src/main/java/br/com/conchayoro/persistence/ProdutoDao.java src/main/java/br/com/conchayoro/service/ProdutoService.java

echo "Gerando instalável..."
docker-compose run --rm java jar cvf conchayoro.war -C .build .